import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.scss";
import CDrawer from "@/components/CDrawer";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Note Todo",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <CDrawer>{children}</CDrawer>
      </body>
    </html>
  );
}
