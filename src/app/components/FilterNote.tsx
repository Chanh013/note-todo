import SwapVertIcon from "@mui/icons-material/SwapVert";
import DoneAllIcon from "@mui/icons-material/DoneAll";
import CMenu from "@/components/CMenu";
import { CTextField } from "@/components/CTextField";
import { Box, Stack, useMediaQuery } from "@mui/material";
import React, { useEffect } from "react";
import { Note } from "@/types/model";
import { useTheme } from "@mui/material/styles";

type FilterNoteProps = {
  setListTodoFilter: React.Dispatch<React.SetStateAction<any[]>>;
  setValueInputSearch: React.Dispatch<React.SetStateAction<string>>;
  listTodo: Note[];
  valueInputSearch?: string;
};

const FilterNote = (props: FilterNoteProps) => {
  const theme = useTheme();
  const isMdScreen = useMediaQuery(theme.breakpoints.up("md"));
  const { setListTodoFilter, listTodo, setValueInputSearch } = props;
  const [sortSelected, setSortSelected] =
    React.useState<string>("Most recently");
  const [typeSelected, setTypeSelected] = React.useState<string>("All");

  const handleSort = (title: string) => {
    setSortSelected(title);
    if (title === "Most recently") {
      setListTodoFilter((prev) => [
        ...prev.sort((a, b) => {
          return (
            new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
          );
        }),
      ]);
    } else {
      setListTodoFilter((prev) => [
        ...prev.sort((a, b) => {
          return (
            new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
          );
        }),
      ]);
    }
  };
  const handleFilterCompleted = (title: string) => {
    setTypeSelected(title);
    if (title === "All") {
      setListTodoFilter([...listTodo]);
    } else if (title === "Completed") {
      setListTodoFilter([
        ...listTodo.filter((note) => note.isComplete === true),
      ]);
    } else {
      setListTodoFilter([
        ...listTodo.filter((note) => note.isComplete === false),
      ]);
    }
  };
  const handelSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValueInputSearch(e.target.value);
    let debound;
    clearTimeout(debound);
    debound = setTimeout(() => {
      if (e.target.value === "") {
        setListTodoFilter([...listTodo]);
        return;
      }
      setListTodoFilter(
        listTodo.filter((note) =>
          note.title.toLowerCase().includes(e.target.value.toLowerCase())
        )
      );
    }, 800);
  };

  return (
    <Box>
      <Stack
        direction={isMdScreen ? "row" : "column"}
        spacing={2}
        justifyContent="space-between"
      >
        <Box sx={{ width: isMdScreen ? "50%" : "100%" }}>
          <CTextField
            label="Enter name note"
            variant="outlined"
            onChance={(e) => {
              setValueInputSearch(e.target.value);
              handelSearch(e);
            }}
          />
        </Box>
        <Box>
          <Stack direction="row" spacing={2}>
            <CMenu
              labelButton="Sort"
              listItems={[
                {
                  title: "Most recently",
                  func: () => handleSort("Most recently"),
                },
                {
                  title: "Last recently",
                  func: () => handleSort("Last recently"),
                },
              ]}
              keySelected={sortSelected}
              endIcon={<SwapVertIcon />}
            />
            <CMenu
              labelButton="Type"
              listItems={[
                {
                  title: "All",
                  func: () => handleFilterCompleted("All"),
                },
                {
                  title: "Completed",
                  func: () => handleFilterCompleted("Completed"),
                },
                {
                  title: "UnCompleted",
                  func: () => handleFilterCompleted("UnCompleted"),
                },
              ]}
              keySelected={typeSelected}
              endIcon={<DoneAllIcon />}
            />
          </Stack>
        </Box>
      </Stack>
    </Box>
  );
};

export default FilterNote;
