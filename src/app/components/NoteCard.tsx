"use client";
import { Box, IconButton, Stack, Typography } from "@mui/material";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";
import AccessAlarmsIcon from "@mui/icons-material/AccessAlarms";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import React, { useRef } from "react";
import CMenu from "@/components/CMenu";
import CheckIcon from "@mui/icons-material/Check";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import RemoveDoneIcon from "@mui/icons-material/RemoveDone";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { Note } from "@/types/model";
import dayjs from "dayjs";
import { FORTMAT_DATE, FORTMAT_DATE_TIME } from "@/constants";
import { text } from "stream/consumers";

type NoteCardProps = {
  note: Note;
  handleOpenModal: (note: Note) => void;
  setListTodo: React.Dispatch<React.SetStateAction<Note[]>>;
};

const NoteCard = (props: NoteCardProps) => {
  const { note, setListTodo, handleOpenModal } = props;
  const menuMoreRef = useRef<any>(null);

  const handleRemoveNote = () => {
    setListTodo((prev) => prev.filter((item) => item.id !== note.id));
  };

  const handleCompleteNote = () => {
    setListTodo((prev) =>
      prev.map((item) => {
        if (item.id === note.id) {
          return { ...item, isComplete: !item.isComplete };
        }
        return item;
      })
    );
  };

  const handleEditNote = (note: Note) => {
    handleOpenModal(note);
  };
  return (
    <Box
      sx={{
        position: "relative",
        background: `${note.isComplete ? "#e8e3e3a8" : "#fff"}`,
        borderRadius: "6px",
        boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
        padding: "20px 25px",
      }}
    >
      <Typography
        variant="subtitle1"
        sx={{
          fontWeight: 600,
          display: "flex",
          alignItems: "center",
          gap: 1,
          lineBreak: "anywhere",
        }}
      >
        {note.isComplete ? <CheckCircleIcon sx={{ color: "#17cc20" }} /> : ""}
        {note.title}
      </Typography>
      <Typography
        variant="caption"
        sx={{ color: "#938888", lineBreak: "anywhere" }}
      >
        {note.description}
      </Typography>
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        sx={{ marginTop: "10px" }}
      >
        <Typography
          variant="caption"
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            gap: "5px",
          }}
        >
          <AccessAlarmsIcon sx={{ fontSize: "18px" }} />
          {dayjs(note.createdAt).format(FORTMAT_DATE_TIME)}
        </Typography>
        <Box>
          <Typography
            variant="caption"
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              gap: "3px",
            }}
          ></Typography>

          <Box sx={{ position: "absolute", top: 2, right: 6 }}>
            <CMenu
              ref={menuMoreRef}
              listItems={[
                {
                  title: note.isComplete ? "Uncomplete" : "Complete",
                  icon: note.isComplete ? <RemoveDoneIcon /> : <CheckIcon />,
                  func: handleCompleteNote,
                },
                {
                  title: "Edit",
                  icon: <EditIcon />,
                  func: () => handleEditNote(note),
                },
                {
                  title: "Remove",
                  icon: <DeleteIcon />,
                  func: handleRemoveNote,
                },
              ]}
            >
              <IconButton
                aria-label="delete"
                size="small"
                onClick={(e) => menuMoreRef.current?.handleOpen(e)}
              >
                <MoreHorizIcon fontSize="small" />
              </IconButton>
            </CMenu>
          </Box>
        </Box>
      </Stack>
    </Box>
  );
};

export default NoteCard;
