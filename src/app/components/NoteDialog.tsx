/* eslint-disable react/display-name */
import CButton from "@/components/CButton";
import CDialog from "@/components/CDialog";
import CTextArea from "@/components/CTextArea";
import { CTextField } from "@/components/CTextField";
import { Note } from "@/types/model";
import { Box, Checkbox, FormControlLabel, Grid, Stack } from "@mui/material";
import React, { forwardRef, useImperativeHandle, useRef } from "react";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { v4 as uuidv4 } from "uuid";

type Inputs = {
  title: string;
  description: string;
  isComplete: boolean;
};
type NoteDialogProps = {
  setListTodo: React.Dispatch<React.SetStateAction<Note[]>>;
};
const NoteDialog = forwardRef((props: NoteDialogProps, ref) => {
  const [noteSelected, setNoteSelected] = React.useState<Note | null>(null);
  const { setListTodo } = props;
  const dialogNoteRef = useRef<any>(null);
  const {
    register,
    handleSubmit,
    reset,
    control,
    formState: { errors },
  } = useForm<Inputs>();

  const handleSubmitNote: SubmitHandler<Inputs> = (data) => {
    if (noteSelected) {
      setListTodo((prev) => [
        ...prev.map((item) => {
          if (item.id === noteSelected.id) {
            return {
              ...item,
              isComplete: !!data.isComplete,
              title: data.title,
              description: data.description,
            };
          }
          return item;
        }),
      ]);
    } else {
      setListTodo((prev) => [
        {
          id: uuidv4(),
          title: data.title,
          description: data.description,
          createdAt: new Date().toString(),
          isComplete: !!data.isComplete,
        },
        ...prev,
      ]);
    }
    handleClose();
  };

  const handleClose = async () => {
    await reset();
    dialogNoteRef.current?.handleClose();
    setNoteSelected(null);
  };

  useImperativeHandle(ref, () => ({
    handleOpen: () => {
      reset({ title: "", description: "" });
      dialogNoteRef.current?.handleOpen();
      setNoteSelected(null);
    },
    handleOpenEdit: (value: Note) => {
      setNoteSelected(value);
      reset({
        title: value?.title,
        description: value?.description,
        isComplete: value?.isComplete,
      });
      dialogNoteRef.current?.handleOpen();
    },
  }));
  return (
    <CDialog
      ref={dialogNoteRef}
      childOpen
      dialogTitle={noteSelected ? "Edit Note" : "Create Note"}
    >
      <Box sx={{ marginTop: 1 }}>
        <form defaultValue="" onSubmit={handleSubmit(handleSubmitNote)}>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          >
            <Grid item xs={3}>
              Title:
            </Grid>
            <Grid item xs={9}>
              <CTextField
                error={!!errors.title}
                helperText="Title is required"
                name="title"
                register={{ ...register("title", { required: true }) }}
                label="Enter your title note"
              />
            </Grid>
            <Grid item xs={3}>
              Description:
            </Grid>
            <Grid item xs={9}>
              <CTextArea
                name="description"
                register={{ ...register("description") }}
                placeholder="Enter your description"
              />
            </Grid>
            <Grid item xs={3}>
              Complete:
            </Grid>
            <Grid item xs={9}>
              <FormControlLabel
                control={
                  <Controller
                    name="isComplete"
                    control={control}
                    render={({ field: props }) => (
                      <Checkbox
                        {...props}
                        checked={!!props.value}
                        onChange={(e) => props.onChange(e.target.checked)}
                        sx={{ paddingLeft: 0, paddingTop: 0 }}
                      />
                    )}
                  />
                }
                label=""
              />
            </Grid>
          </Grid>
          <Stack
            direction="row"
            justifyContent="flex-end"
            spacing={2}
            sx={{ marginTop: 2 }}
          >
            <CButton variant="contained" type="submit">
              {noteSelected ? "Update" : "Add"}
            </CButton>
            <CButton variant="outlined" onClick={handleClose}>
              Cancel
            </CButton>
          </Stack>
        </form>
      </Box>
    </CDialog>
  );
});

export default NoteDialog;
