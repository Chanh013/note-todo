"use client";
import {
  Backdrop,
  Box,
  CircularProgress,
  Grid,
  IconButton,
  Typography,
} from "@mui/material";
import * as React from "react";
import FilterNote from "./components/FilterNote";
import NoteCard from "./components/NoteCard";
import CMenu from "@/components/CMenu";
import AddIcon from "@mui/icons-material/Add";
import LibraryAddIcon from "@mui/icons-material/LibraryAdd";
import LocalOfferIcon from "@mui/icons-material/LocalOffer";
import NoteDialog from "./components/NoteDialog";
import { Note } from "@/types/model";
import Image from "next/image";
import NotValueImg from "../../public/assets/images/no-data-found.png";

export default function Home() {
  const menuOptionRef = React.useRef<any>(null);
  const dialogNoteRef = React.useRef<any>(null);
  const [listTodo, setListTodo] = React.useState<Note[]>([]);
  const [listTodoFilter, setListTodoFilter] = React.useState<Note[]>([]);
  const [isSetLocalStore, setIsSetLocalStore] = React.useState<boolean>(false);
  const [valueInputSearch, setValueInputSearch] = React.useState("");

  React.useEffect(() => {
    setListTodo(JSON.parse(localStorage.getItem("listTodo") || "[]") ?? []);
    setIsSetLocalStore(true);
  }, []);

  React.useEffect(() => {
    if (isSetLocalStore) {
      localStorage.setItem("listTodo", JSON.stringify(listTodo));
      if (valueInputSearch) {
        setListTodoFilter(
          listTodo.filter((note) =>
            note.title.toLowerCase().includes(valueInputSearch.toLowerCase())
          )
        );
      } else setListTodoFilter(listTodo);
    }
  }, [listTodo, isSetLocalStore]);

  const handleOpenNoteDialog = () => {
    dialogNoteRef.current?.handleOpen();
  };

  return (
    <Box sx={{ width: "100%", position: "relative", height: "100%" }}>
      <FilterNote
        setValueInputSearch={setValueInputSearch}
        setListTodoFilter={setListTodoFilter}
        valueInputSearch={valueInputSearch}
        listTodo={listTodo}
      />
      {!isSetLocalStore ? (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100%",
          }}
        >
          <CircularProgress color="inherit" />
        </Box>
      ) : listTodo.length === 0 ? (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100%",
          }}
        >
          <Image width={530} height={280} src={NotValueImg} alt="" />
        </Box>
      ) : (
        <Box sx={{ marginTop: "20px" }}>
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            {listTodoFilter.map((note: Note, index) => (
              <Grid item xs={8} md={4} sm={8} key={index}>
                <NoteCard
                  note={note}
                  setListTodo={setListTodo}
                  handleOpenModal={dialogNoteRef.current?.handleOpenEdit}
                />
              </Grid>
            ))}
          </Grid>
        </Box>
      )}

      <Box sx={{ position: "fixed", bottom: 10, right: 10 }}>
        <CMenu
          ref={menuOptionRef}
          listItems={[
            {
              title: "Add note",
              icon: <LibraryAddIcon />,
              func: handleOpenNoteDialog,
            },
            { title: "New Tag", icon: <LocalOfferIcon /> },
          ]}
        >
          <IconButton
            size="large"
            onClick={(e) => menuOptionRef.current?.handleOpen(e)}
            style={{ background: "rgb(185 181 181 / 38%)" }}
          >
            <AddIcon fontSize="large" />
          </IconButton>
        </CMenu>
      </Box>
      <NoteDialog setListTodo={setListTodo} ref={dialogNoteRef} />
    </Box>
  );
}
