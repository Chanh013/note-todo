export type Note = {
  id: string;
  title: string;
  description?: string;
  createdAt: string;
  isComplete: boolean;
};
