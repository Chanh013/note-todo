import { TextareaAutosize as BaseTextareaAutosize } from "@mui/base/TextareaAutosize";
import { styled } from "@mui/system";
import React from "react";

const Textarea = styled(BaseTextareaAutosize)(
  () => `
    width: 100%;
    font-family: 'IBM Plex Sans', sans-serif;
    font-size: 0.875rem;
    font-weight: 400;
    line-height: 1.5;
    padding: 8px 12px;
    border-radius: 4px;
    &:focus-visible {
      outline: 0;
    }
  `
);

type CTextAreaProps = {
  placeholder?: string;
  name?: string;
  register?: any;
};
const CTextArea = (props: CTextAreaProps) => {
  const { placeholder, name, register } = props;
  return (
    <Textarea {...register} minRows={3} placeholder={placeholder} name={name} />
  );
};

export default CTextArea;
