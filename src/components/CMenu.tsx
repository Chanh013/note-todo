/* eslint-disable react/display-name */
"use client";
import { Box, ListItemIcon, ListItemText, Menu, MenuItem } from "@mui/material";
import React, { forwardRef, useImperativeHandle } from "react";
import CButton from "./CButton";

type CMenuProps = {
  labelButton?: string;
  listItems: {
    title: string;
    icon?: React.ReactNode;
    func?: (...args: any[]) => void;
  }[];
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
  children?: React.ReactNode;
  keySelected?: string;
};
const CMenu = forwardRef((props: CMenuProps, ref) => {
  const { labelButton, listItems, endIcon, startIcon, children, keySelected } =
    props;
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleOpen = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  useImperativeHandle(ref, () => ({
    handleOpen,
  }));
  return (
    <Box>
      {children ?? (
        <CButton
          startIcon={startIcon ?? ""}
          endIcon={endIcon ?? ""}
          onClick={handleOpen}
        >
          {labelButton}
        </CButton>
      )}

      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        {listItems?.map((item, index) => (
          <MenuItem
            key={index}
            selected={keySelected === item.title ? true : false}
            onClick={() => {
              item.func?.();
              handleClose();
            }}
          >
            {item.icon && <ListItemIcon>{item.icon}</ListItemIcon>}
            <ListItemText>{item.title}</ListItemText>
          </MenuItem>
        ))}
      </Menu>
    </Box>
  );
});

export default CMenu;
