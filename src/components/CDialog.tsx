/* eslint-disable react/display-name */
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import React, { forwardRef, useImperativeHandle } from "react";
import CButton from "./CButton";

type CDialogProps = {
  labelButton?: string;
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
  variant?: "outlined" | "contained";
  childOpen?: React.ReactNode;
  dialogTitle?: string;
  children?: React.ReactNode;
};
const CDialog = forwardRef((props: CDialogProps, ref) => {
  const {
    labelButton,
    endIcon,
    startIcon,
    variant,
    childOpen,
    dialogTitle,
    children,
  } = props;
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  useImperativeHandle(ref, () => ({
    handleOpen,
    handleClose,
  }));

  return (
    <React.Fragment>
      {childOpen ?? (
        <CButton
          startIcon={startIcon}
          endIcon={endIcon}
          variant={variant}
          onClick={handleOpen}
        >
          {labelButton}
        </CButton>
      )}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        {dialogTitle ? (
          <DialogTitle id="alert-dialog-title">{dialogTitle}</DialogTitle>
        ) : (
          ""
        )}
        <DialogContent>{children}</DialogContent>
      </Dialog>
    </React.Fragment>
  );
});

export default CDialog;
