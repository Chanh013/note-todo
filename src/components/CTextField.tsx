"use client";
import { TextField, TextFieldProps, styled } from "@mui/material";
import React from "react";

type CTextFieldProps = {
  label?: string;
  variant?: "standard" | "filled" | "outlined";
  name?: string;
  register?: any;
  error?: boolean;
  helperText?: string;
  onChance?: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

const MuiTextField = styled(TextField)<TextFieldProps>(({ theme }) => ({
  width: "100%",
  ".MuiInputBase-input": {
    fontSize: "14px",
    padding: "12px 14px",
  },
  ".MuiInputLabel-root": {
    fontSize: "14px",
    transform: "translate(14px, 13px)",
  },
  ".Mui-focused.MuiInputLabel-root": {
    transform: "translate(14px, -9px) scale(0.75)",
  },
  ".MuiInputLabel-shrink": {
    transform: "translate(14px, -9px) scale(0.75) !important",
  },
}));

export const CTextField = (props: CTextFieldProps) => {
  const {
    label,
    variant,
    name,
    register,
    error = false,
    helperText,
    onChance,
  } = props;
  return (
    <MuiTextField
      {...register}
      id="standard-basic"
      label={label}
      variant={variant}
      name={name}
      error={error}
      helperText={error && helperText ? helperText : ""}
      onChange={onChance}
    />
  );
};
